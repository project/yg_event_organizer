(function($) {
	$(document).ready(function() {
		"use strict";
		
});
	    $(document).click(function(e) {
		    if (!$(e.target).is('.panel-body')) {
		      $('.collapse').collapse('hide');      
		    }
		    });

		 jQuery(window).scroll(function(){
	          var scroll = $(window).scrollTop();
	          if (scroll >= 100) {
	              $("#main-menu").addClass("sticky");
	          } else {
	              $("#main-menu").removeClass("sticky");
	          }

	          });
		jQuery(document).ready(function( $ ) {	



				  $('.popup-youtube').magnificPopup({
				         disableOn: 700,
				         type: 'iframe',
				         mainClass: 'mfp-fade',
				         removalDelay: 160,
				         preloader: false,
				         fixedContentPos: false
				       });


						$('.counter').counterUp({
						                delay: 10,
						                time: 1000
						            });


						$(".service-carousel").owlCarousel({
				        dots: false,
				        pagination: true,
				        autoPlay:true,
						responsive: {
					      0:{
					          items:1
					      },
					      600:{
					          items:2
					      },
					      1000:{
					          items:4
					      }    
					    }
				    });

				$('.portfolio-popup').magnificPopup({
			    type: 'image',
			    removalDelay: 300,
			    mainClass: 'mfp-fade',
			    gallery: {
			      enabled: true
			    },
			    zoom: {
			      enabled: false,
			      duration: 300,
			      easing: 'ease-in-out',
			      opener: function(openerElement) {
			        return openerElement.is('img') ? openerElement : openerElement.find('img');
			      }
			    }

				});



				$(".performers-carousel").owlCarousel({
				        
				        pagination: true,
				        autoPlay:true,
						responsive: {
					      0:{
					          items:1
					      },
					      600:{
					          items:2
					      },
					      1000:{
					          items:4
					      }    
					    }
				    });
				 $('.clients').owlCarousel({
				    loop: true,
				    margin: 10,
				    responsive: {
				      0:{
				            items:1
				        },
				        600:{
				            items:1
				        },
				        1000:{
				            items:1
				        }    
				    }
  					});

				});	

		 

})(jQuery);
